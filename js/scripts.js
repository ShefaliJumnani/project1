$(document).ready(function() {
    $('#mycarousel').carousel(
        {
            interval:2000,
            cycle:"true",
            pause:"null" 
        }
    );

    $('#carouselButtonx').click(function() {
        if ($('#carouselButtonx').children('span').hasClass('fa-pause'))
        {
            $('#mycarousel').carousel('pause');
            $('#carouselButtonx').children('span').removeClass('fa-pause');
            $('#carouselButtonx').children('span').addClass('fa-play');
        }
        else if ($('#carouselButtonx').children('span').hasClass('fa-play'))
        {
            $('#mycarousel').carousel('cycle');
            $('#carouselButtonx').children('span').removeClass('fa-play');
            $('#carouselButtonx').children('span').addClass('fa-pause');
        }
    });
    $('#button1').click(function(){
        $('#reservation-form').modal('toggle');
    });
    $('#button2').click(function(){
        $('#loginModal').modal('toggle');
    });
});